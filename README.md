# PRIMEIRA ATIVIDADE DA AULA DE HTML



## Integrante

Leandro Petito e Leonardo Calvet



## Descrição

Um integrante do grupo vai criar um repositório no GitLab e adicionar os outros. (Lembre-se de dar permissão);

Cada integrante do grupo vai criar uma Branch para desenvolver sua página individual;

Para cada funcionalidade ou alteração usem commit;

Quando a parte individual estiver pronta, de merge com a master;

Lembre-se do push e do pull, vocês vão está trabalhando em equipe, então o código precisa sempre estar atualizado;

No final a Branch master precisa ter a página principal e a página individual de cada um.